(function($) {
    // replaces paper.js objects with the required properties only.
    // 'shapes' coordinates are image coordiantes
    $.Overlay.prototype.replaceShape = function(shape, annotation) {

        //backward compatibility when changing shapes points
        var shapeTool = this.getTool(shape);
        if(shapeTool && shapeTool.SEGMENT_POINTS_COUNT && shapeTool.SEGMENT_POINTS_COUNT !== shape.segments.length){
          shapeTool.adaptSegments(shape,this);
        }
  
        var cloned = new this.paperScope.Path({
          segments: shape.segments,
          name: shape.name
        });
  
        // NEW/ENHANCEMENT: Support for CompoundPaths
        if (shape.className === 'CompoundPath') {
          cloned = new this.paperScope.CompoundPath({
            children: shape._children,
            name: shape.name,
            closed: false
          });
        }
        cloned.data.strokeWidth = shape.data.strokeWidth || 1;
        cloned.strokeWidth = cloned.data.strokeWidth / this.paperScope.view.zoom;
        cloned.strokeColor = shape.strokeColor;
        cloned.dashArray = shape.dashArray;
        if (shape.fillColor) {
          cloned.fillColor = shape.fillColor;
  
          // workaround for paper js fill hit test
          if(shape.fillColor.alpha === 0){
            shape.fillColor.alpha = FILL_COLOR_ALPHA_WORKAROUND;
          }
          if (shape.fillColor.alpha) {
            cloned.fillColor.alpha = shape.fillColor.alpha;
          }
        }
        //cloned.closed = shape.closed;
        cloned.closed = (shape.className === 'CompoundPath') ? false : shape.closed;
        cloned.data.rotation = shape.data.rotation;
        cloned.data.fixedSize = shape.data.fixedSize;
        cloned.data.annotation = annotation;
        if (cloned.data.fixedSize) {
          this.fitFixedSizeShapes(cloned);
        }
        shape.remove();
        return cloned;
      }
    }(Mirador));