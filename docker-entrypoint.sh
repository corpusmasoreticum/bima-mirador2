#!/bin/sh

mkdir /tmp/mirador && cd /tmp/mirador
curl -s https://api.github.com/repos/ProjectMirador/mirador/releases/14911692 \
    | jq '.assets[] | select(.name == "build.tar.gz") | .browser_download_url' \
    | xargs curl -sLO
tar xfz build.tar.gz
curl -OJL https://github.com/ProjectMirador/mirador/raw/develop/index.html
cp -arv /tmp/mirador/build/ /usr/share/nginx/html/build
cp -arv /tmp/mirador/index.html /usr/share/nginx/html/index.html
tail -f /dev/null